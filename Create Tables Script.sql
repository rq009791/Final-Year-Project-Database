/*
DROP TABLE Rule_Parameter;
DROP TABLE Rule;
DROP TABLE Rule_Action;
DROP TABLE Geozone_Coordinates;
DROP TABLE Geozone;
DROP TABLE Authorised_Users;
DROP TABLE GPS_Endpoint_Occurance;
DROP TABLE GPS_Endpoints;
DROP TABLE GPS_Readings;
DROP TABLE Coordinate;
DROP TABLE Heart_Rate_Reading;
DROP TABLE Sent_Notifications;
DROP TABLE Notification;
DROP TABLE Acceleromiter_Reading;
DROP TABLE Users;
*/
-- https://www.w3schools.com/sql/sql_autoincrement.asp   -> 03/12/2017  -> Used to identify the format for an autoincrementing primary key (surrogate key)

CREATE TABLE Users(
	UserID INT PRIMARY KEY AUTO_INCREMENT,
    Password CHAR(64),
    Salt BINARY(64),
    User_Type VARCHAR(16)
);


CREATE TABLE Acceleromiter_Reading(
	Acceleromiter_ReadingID INT PRIMARY KEY AUTO_INCREMENT,
    Reading DOUBLE,
    Stamp DATETIME,
    UserID INT,
    Analysed BOOLEAN, -- Added from the initial schema design to make sure the API does not identify the same issue more than once.
    FOREIGN KEY FK_Acceleromiter_Reading__Users (UserID) REFERENCES Users(UserID)
);

CREATE TABLE Notification(
	NotificationID INT PRIMARY KEY AUTO_INCREMENT,
    Title VARCHAR(45),
    Notification VARCHAR(255),
    Notification_Type VARCHAR(16),
    UserID INT,
    FOREIGN KEY FK_Notification__Users (UserId) REFERENCES Users(UserID)
);


CREATE TABLE Sent_Notifications(
	Sent_NotificationsID INT PRIMARY KEY AUTO_INCREMENT,
    Sent_At DATETIME,
    NotificationID INT,
    UserID INT,
	Stamp DATETIME,
    FOREIGN KEY FK_Sent_Notifications__Notification (NotificationID) REFERENCES Notification(NotificationID),
    FOREIGN KEY FK_Sent_Notifications__Users (UserID) REFERENCES Users(UserID)
);


CREATE TABLE Heart_Rate_Reading(
	Heart_Rate_ReadingID INT PRIMARY KEY AUTO_INCREMENT,
    Stamp DATETIME,
    Reading INT, -- This is in beats per minute
    UserID INT,
	Analysed BOOLEAN, -- Added from the initial schema design to make sure the API does not identify the same issue more than once.
    FOREIGN KEY FK_Heart_Rate_Reading__Users (UserID) REFERENCES Users(UserID)
);


-- Tables for the GPS data 
CREATE TABLE Coordinate(
	CoordinateID INT PRIMARY KEY AUTO_INCREMENT,
    Longitude DECIMAL(9,6),
    Latitude DECIMAL(8,6)
);

CREATE TABLE GPS_Readings(
    GPS_ReadingsID INT PRIMARY KEY AUTO_INCREMENT,
    Stamp DATETIME,
    UserID INT,
    CoordinateID INT,
    Analysed BOOLEAN, -- Added from the initial schema design to make sure the API does not identify the same issue more than once.
    FOREIGN KEY FK_GPS_Readings__Users (UserID) REFERENCES Users(UserID),
    FOREIGN KEY FK_GPS_Readings__Coordinate (CoordinateID) REFERENCES Coordinate(CoordinateID) ON DELETE CASCASE
);







CREATE TABLE Authorised_Users(
	Authorised_UsersID INT PRIMARY KEY AUTO_INCREMENT,
    Authorised_Time DATETIME, -- THIS HAS BEEN ADDED FORM THE INITIAL SCHEMA BECAUSE IT WAS BELIEVED TO BE BENEFITIAL
    Expiry_Time DATETIME,
	Monitored_UserID INT,
    Authorised_UserID INT,
    Relation char(1),
    DataTable char(1),
    FOREIGN KEY FK_Authorised_Users_Monitored_User__Users (Monitored_UserID) REFERENCES Users(UserID),
    FOREIGN KEY FK_Authorised_Users_AuthorisedUser__Users (Authorised_UserID) REFERENCES Users(UserID)
);




/*
		The following section of the database is intended to implement extra features such as geozones
*/
-- geozones -> this highly links to the Rule Engine

CREATE TABLE Geozone(
	GeozoneID INT PRIMARY KEY AUTO_INCREMENT,
    Zone_Name VARCHAR(50),
    UserID INT,
	IsSystemGenerated BOOLEAN DEFAULT FALSE,
    WestMostLongitude DECIMAL(9,6),
    EastMostLongitude DECIMAL(9,6),
    NorthMostLatitude DECIMAL(8,6),
    SouthMostLatitude DECIMAL(8,6),
	FOREIGN KEY FK_Geozone__User (UserID) REFERENCES Users(UserID)
);

CREATE TABLE GPS_Endpoints(
	GPS_EndpointsID INT PRIMARY KEY AUTO_INCREMENT,
    Point_A INT,
    Point_B INT,
    UserID INT,
    FOREIGN KEY FK_GPS_Endpoints__Users (UserID) REFERENCES Users(UserID),
    FOREIGN KEY FK_GPS_Endpoints__Geozone_PointA (Point_A) REFERENCES Geozone(GeozoneID),
    FOREIGN KEY FK_GPS_Endpoints__Geozone_PointB (Point_B) REFERENCES Geozone(GeozoneID)
);

CREATE TABLE GPS_Endpoint_Occurance(-- THIS TABLE IS USED TO MINIMISE REPETITION OF THE METADATA, ONLY RECORDING THE DATE AND TIME EACH TIME A JOURNEE IS TAKEN
	GPS_Endpoint_OccuranceID INT PRIMARY KEY AUTO_INCREMENT,
    Stamp DATETIME,
    GPS_EndpointsID INT,
    Analysed BOOLEAN NOT NULL DEFAULT FALSE, -- ADDED AS I REALISED THAT IT IS WORTH WHIILE KNOWING WHICH ITEMS HAVE BEEN PROCESSED HERE
    FOREIGN KEY FK_GPS_Endpoint_Occurance__GPS_Endpoints (GPS_EndpointsID) REFERENCES GPS_Endpoints (GPS_EndpointsID)
);
-- RULE ENGINE TABLES
--
CREATE TABLE Rule_Action(
	Rule_ActionID INT PRIMARY KEY AUTO_INCREMENT,
    Action_Name VARCHAR(50), -- Changed the name slightly as Name is a key word
    Description VARCHAR(500),
    Command VARCHAR(20)
);

CREATE TABLE Rule(
	RuleID INT PRIMARY KEY AUTO_INCREMENT,
    Rule_Name VARCHAR(50), -- Changed name as 'Name' is a key word in MySQL. The number of characters has also been rounded up to be a similar number used elsewhere in the schema
    Rule_ActionID INT,
    DataTable varchar(20),
    Created_By INT,
    Action_On INT,
    Authorised BOOLEAN, -- This may be changed to a logging table in future as it may be good to long authorised time stamps and the corresponding time stamps
    Expire_Stamp DATETIME,
    FOREIGN KEY FK_Rule__Rule_Action (Rule_ActionID) REFERENCES Rule_Action(Rule_ActionID),
    FOREIGN KEY FK_Rule_Created_By__User (Created_By) REFERENCES Users(UserID),
    FOREIGN KEY FK_Rule_Action_On__User (Action_On) REFERENCES Users(UserID)
);

/*
References used to identify how the complex contraignt can be achieved
https://dev.mysql.com/doc/refman/5.7/en/multiple-column-indexes.html - > 03/12/2017 -> multiple column index

https://stackoverflow.com/questions/12763726/mysql-make-a-pair-of-values-unique -> 03/12/2017 -> Raphaël Althaus -> Composite key
*/

CREATE TABLE Rule_Parameter(
	Rule_ParameterID INT PRIMARY KEY AUTO_INCREMENT,
    Parameter_Index INT, -- Renamed as 'Index' is a keyword in MySQL
    Parameter VARCHAR(200),
    Parameter_Type VARCHAR(20),
    RuleID INT,
    FOREIGN KEY FK_Rule_Parameter__Rule (RuleID) REFERENCES Rule(RuleID),
    UNIQUE KEY (RuleID, Parameter_Index) -- Used to make sure the same index is not used more than once per rule
);
