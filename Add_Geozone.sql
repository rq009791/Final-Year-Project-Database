drop procedure Add_Geozone;
DELIMITER $$

CREATE PROCEDURE Add_Geozone (IN user_id INT, zone_name_in VARCHAR(50), is_system_generated BOOLEAN, IN coordinate1_latitude DECIMAL(8,6) , IN coordinate1_longitude DECIMAL(9,6),  
											IN coordinate2_latitude DECIMAL(8,6) , IN coordinate2_longitude DECIMAL(9,6),
                                            IN coordinate3_latitude DECIMAL(8,6) , IN coordinate3_longitude DECIMAL(9,6),
                                            IN coordinate4_latitude DECIMAL(8,6) , IN coordinate4_longitude DECIMAL(9,6))
                                            
BEGIN

	DECLARE geozone_id INT;

	INSERT INTO geozone(Zone_Name, UserID, IsSystemGenerated)
    VALUES (zone_name_in, user_id, is_system_generated);
    
    SET geozone_id = LAST_INSERT_ID();
    
    CALL Add_Geozone_Coordinate(geozone_id, coordinate1_latitude, coordinate1_longitude);
	CALL Add_Geozone_Coordinate(geozone_id, coordinate2_latitude, coordinate2_longitude);
    CALL Add_Geozone_Coordinate(geozone_id, coordinate3_latitude, coordinate3_longitude);
    CALL Add_Geozone_Coordinate(geozone_id, coordinate4_latitude, coordinate4_longitude);
    
    COMMIT;
END ;$$
