 drop procedure Delete_Account;
DELIMITER $$

CREATE PROCEDURE Delete_Account (IN user_id INT)
                                            
BEGIN

    DELETE FROM Sent_Notifications WHERE UserID = user_id;
    DELETE FROM Notification WHERE UserID = user_id;
    
        
    DELETE FROM Heart_Rate_Reading WHERE UserID = userID;
    
    DELETE FROM Acceleromiter_Reading WHERE UserID = user_id;
    
    DELETE FROM GPS_Endpoint_Occurance WHERE GPS_EndpointsID IN (SELECT GPS_EndpointsID FROM GPS_Endpoints WHERE UserID = user_id);
    
    
    DELETE FROM GPS_Endpoints WHERE UserID = user_id;
    DELETE FROM Geozone WHERE UserID = user_id;
    
    DELETE FROM Rule_Parameter WHERE RuleID  IN (SELECT RuleID FROM Rule WHERE Created_By = user_id OR Action_On = user_id);
    DELETE  FROM Rule WHERE Created_By = user_id OR Action_On = user_id;
    
    DELETE FROM Coordinate WHERE CoordinateID IN (SELECT CoordinateID FROM GPS_Readings WHERE UserID = userID);
    DELETE FROM GPS_Readings WHERE UserID = userID;
    
    DELETE FROM Authorised_Users WHERE Monitored_UserID = user_id OR Authorised_UserID = user_id;
    DELETE FROM Users WHERE UserID = user_id;
    
    COMMIT;
END ;$$
