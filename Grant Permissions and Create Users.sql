-- https://dev.mysql.com/doc/refman/5.7/en/create-user.html (13/12/2017)
CREATE USER IF NOT EXISTS api_user IDENTIFIED BY 'development_password';

-- granting the API the minimum permission set required to forfill the purpose 
GRANT SELECT, INSERT ON Users TO api_user;
GRANT SELECT, INSERT ON Sent_Notifications TO api_user;
GRANT SELECT, INSERT, UPDATE ON Rule TO api_user;
GRANT SELECT ON Rule_Action TO api_user;
GRANT SELECT, INSERT ON Rule_Parameter TO api_user;
GRANT SELECT, INSERT ON Notification TO api_user;
GRANT SELECT, INSERT, UPDATE ON Heart_Rate_Reading TO api_user;
GRANT SELECT, INSERT, UPDATE ON GPS_Readings TO api_user;
GRANT SELECT, INSERT ON GPS_Endpoints TO api_user;
GRANT SELECT, INSERT, UPDATE ON GPS_Endpoint_Occurance TO api_user; 
--GRANT SELECT, INSERT ON Geozone_Coordinates TO api_user;
GRANT SELECT, INSERT ON Geozone TO api_user;
GRANT SELECT, INSERT ON Coordinate TO api_user;
GRANT SELECT, INSERT, DELETE, UPDATE ON Authorised_Users TO api_user;
GRANT SELECT, INSERT,UPDATE ON Acceleromiter_Reading TO api_user;
--GRANT EXECUTE ON PROCEDURE Add_Geozone TO api_user;
--GRANT EXECUTE ON PROCEDURE Add_Geozone_Coordinate TO api_user;
GRANT EXECUTE ON PROCEDURE Delete_System_Generated_Geozones TO api_user;
GRANT EXECUTE ON PROCEDURE Add_Journey TO api_user;
GRANT EXECUTE ON PROCEDURE Delete_Rule TO api_user;
GRANT EXECUTE ON PROCEDURE Delete_Account TO api_user; 

