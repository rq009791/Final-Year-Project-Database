--drop procedure Add_Journey;
DELIMITER $$

CREATE PROCEDURE Add_Journey (IN user_id INT, IN ZoneA INT, IN ZoneB INT,IN destinationArrivalTime DATETIME)
                                            
BEGIN
    
    -- CHECK TO SEE IF THE JOURNEE ALREADY EXISTS FOR THIS USER
    IF( (SELECT COUNT(*) FROM GPS_Endpoints WHERE UserID = user_id AND Point_A = ZoneA AND Point_B = ZoneB) = 0) THEN
		INSERT INTO GPS_Endpoints (Point_A, Point_B, UserID)
        VALUES(ZoneA, ZoneB, user_id);
    END IF;
    
    INSERT INTO GPS_Endpoint_Occurance(Stamp, Analysed ,GPS_EndpointsID )
    VALUES(destinationArrivalTime, FALSE, (SELECT GPS_EndpointsID FROM GPS_Endpoints WHERE UserID = user_id AND Point_A = ZoneA AND Point_B = ZoneB) );
    
    COMMIT;

END ;


$$
