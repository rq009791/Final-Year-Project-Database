drop procedure Add_Geozone_Coordinate;
DELIMITER $$

CREATE PROCEDURE Add_Geozone_Coordinate (IN geozone_id INT, IN latitude_in DECIMAL(8,6) , IN longitude_in DECIMAL(9,6))
                                            
BEGIN

	DECLARE coordinate_id INT;

	INSERT INTO coordinate(Latitude, Longitude)
    VALUES (latitude_in, longitude_in);
    
    SET coordinate_id = LAST_INSERT_ID();
    
    INSERT INTO geozone_coordinates ( CoordinateID, GeozoneID)
    VALUES (coordinate_id,geozone_id);
    
    COMMIT;

END ;

$$
