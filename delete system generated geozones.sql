--drop procedure Delete_System_Generated_Geozones;
DELIMITER $$

CREATE PROCEDURE Delete_System_Generated_Geozones (IN user_id INT)
                                            
BEGIN
    
    DELETE FROM Geozone WHERE UserID = user_id AND IsSystemGenerated = TRUE;
    
    COMMIT;

END ;

$$
