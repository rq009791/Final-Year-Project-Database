DELIMITER $$

CREATE PROCEDURE delete_rule(user_id INTEGER, rule_id INTEGER)



BEGIN

	IF((SELECT COUNT(*) FROM Rule WHERE RuleID = rule_id AND Action_On =  user_id) > 0) THEN
		
       DELETE FROM Rule_Parameter WHERE RuleID = rule_id;
        
		DELETE FROM Rule WHERE RuleID = rule_id;
        
    END IF;


END;
$$
