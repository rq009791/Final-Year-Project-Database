INSERT INTO Users (Password, Salt,User_Type) VALUES('something', 'random salt', 'Monitored');
INSERT INTO rule_action (Action_Name,Description,Command) VALUES('Above', 'Value above a certain amount.', 'ABV');
INSERT INTO rule_action (Action_Name,Description,Command) VALUES('Below', 'Value below a certain amount.', 'BLO');
INSERT INTO Rule (Rule_Name, Rule_ActionID, Created_By, Action_On) VALUES('A cool rule name', (SELECT Rule_ActionID FROM rule_action WHERE Command = 'ABV'), 1, 1);

INSERT INTO heart_rate_reading(Stamp,Reading, UserID, Analysed) VALUES(NOW(),25,1,TRUE), (date_add(NOW(), INTERVAL 1 minute), 26,1,TRUE);

SELECT * FROM heart_rate_reading;


SELECT * FROM authorised_users;
insert INTO Rule_parameter


SELECT * FROM RULE;
update Rule set Authorised = false where RuleID = 2;

select * from Users order by UserID;

INSERT INTO Users (UserID, Password, Salt,User_Type) VALUES(0,'testing', 'testing salt', 'Monitored');


SELECT * FROM rule_parameter where RuleID = 2 order by Parameter_Index; 

INSERT INTO rule_parameter (Parameter_Index, Parameter, Parameter_Type, RuleID) VALUES(0, 'a PARAMETER', 'STRING', 2), (1, '8', 'INT', 2);


SELECT Authorised_UserID FROM Authorised_Users WHERE Monitored_UserID = 5 AND Authorised_Time IS NOT NULL AND Expiry_Time IS NOT NULL AND Expiry_Time >= ?

;
SELECT * from Authorised_Users;

SELECT COUNT(*) as numberOfRows FROM Authorised_Users WHERE Monitored_UserID = 1 AND Authorised_UserID = 2 AND Expiry_Time <= ? -- the last date comparator is the wrong way round


;


SELECT * FROM Heart_Rate_Reading WHERE UserID = 1 AND (Analysed = FALSE OR Stamp >= adddate( now(), interval -1 month) )ORDER BY Stamp;

UPDATE rule SET DataTable = 'HeartRate' 	WHERE Created_By =1;



SELECT * FROM gps_readings;
SELECT * FROM coordinate;

DELETE FROM gps_readings WHERE CoordinateID = 1;

INSERT INTO coordinate (Longitude, Latitude) values (5,7), (100,17);
INSERT INTO gps_readings (Stamp, UserID, CoordinateID, Analysed) VALUES (NOW(), 1, 1, FALSE), (date_add(NOW(), INTERVAL -1 minute), 1, 2, FALSE);



SELECT GeozoneID, Zone_Name, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude FROM geozone WHERE UserID = 1 AND IsSystemGenerated = TRUE;

SELECT c.Latitude, c.Longitude FROM geozone_coordinates g INNER JOIN coordinate c ON g.CoordinateID = c.CoordinateID WHERE g.GeozoneID = ?;

select Zone_Name, UserID, IsSystemGenerated, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude from geozone;

select Zone_Name, GeozoneID, IsSystemGenerated, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude from geozone;

INSERT INTO geozone (Zone_Name, UserID, IsSystemGenerated, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude)
VALUES (?,?,?,?,?,?,?);

SELECT g.GPS_ReadingsID, g.Stamp, g.UserID,  c.Latitude, c.Longitude ,g.Analysed FROM gps_readings g INNER JOIN coordinate c ON g.CoordinateID = c.CoordinateID;

SELECT GPS_EndpointsID 
FROM gps_endpoints 
WHERE UserID = ?

SELECT Stamp
FROM gps_endpoint_occurance
WHERE GPS_EndpointsID = ?
	AND Analysed = TRUE;



INSERT INTO notification (Title,Notification,Notification_Type,UserID) VALUES (?,?,?,?);


SELECT * FROM rule_action;


SELECT RuleID,Rule_Name, Action_Name, Action_On, Authorised, DataTable
FROM rule r INNER JOIN rule_action ra ON r.Rule_ActionID = ra.Rule_ActionID
WHERE Created_By = '';


SELECT Parameter_Index, Parameter, Parameter_Type
FROM rule_parameter 
WHERE RuleID = ?;

SELECT Authorised_Time, Expiry_Time, Monitored_UserID 
FROM authorised_users
WHERE Authorised_UserID = ?;



INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID)
VALUES (NULL, NULL,24,25),(NULL, NULL,24,25),(NULL, NULL,24,25), (now(), date_add(now(), INTERVAL 1 MONTH),24,25);



select * from authorised_users;

update authorised_users  set Relation = 'F', DataTable = 'G' where Authorised_UsersID = 9;
 
 
INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID, Relation, DataTable)
VALUES (now(), date_add(now(), INTERVAL 1 MONTH),23,25, 'F', 'H'),
		(now(), date_add(now(), INTERVAL 1 MONTH),22,25 , 'F', 'A');


INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID, Relation, DataTable)
VALUES (now(), date_add(now(), INTERVAL 1 MONTH),21,25, 'M', 'H'),
		(now(), date_add(now(), INTERVAL 1 MONTH),20,25 , 'M', 'A'),
        (now(), date_add(now(), INTERVAL 1 MONTH),19,25 , 'M', 'G');


INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID, Relation, DataTable)
VALUES (now(), date_add(now(), INTERVAL 1 MONTH),23,25, 'F', 'H'),
		(now(), date_add(now(), INTERVAL 1 MONTH),23,25 , 'F', 'A'),
        (now(), date_add(now(), INTERVAL 1 MONTH),23,25 , 'F', 'G');


INSERT INTO notification (Title,Notification,Notification_Type,UserID)
VALUES ('High Heart Rate', 'The users heartrate is higher than expected based on their movement at <some date here>', 'H', 23),
		('Low Heart Rate', 'The users heartrate is lower than expected based on their movement at <some date here>', 'H', 23),
		('Iregular Behaviour', 'The user always travels to a location on a Wednesday but they have been on a Tuesday on <some date here>', 'G', 23),
		('Prelonged Inacctivity', 'The user has not moved for over a week', 'A', 23)
;

delete from notification where UserID = 23;
delete from authorised_users where Authorised_UsersID = 10; -- removed an accidental duplicate record
select * from notification;
select * from authorised_users where Monitored_UserID = 23;



SELECT * FROM  notification WHERE UserID = 23 AND Notification_Type IN ('H','A','G');

INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID, Relation, DataTable)
VALUES (now(), date_add(now(), INTERVAL 1 MONTH),22,25, 'M', 'H'),
		(now(), date_add(now(), INTERVAL 1 MONTH),22,25 , 'M', 'A'),
        (now(), date_add(now(), INTERVAL 1 MONTH),22,25 , 'M', 'G')
	;




INSERT INTO notification (Title,Notification,Notification_Type,UserID)
VALUES ('High Heart Rate', 'The users heartrate is higher than expected based on their movement at <some date here>', 'H', 22),
		('Low Heart Rate', 'The users heartrate is lower than expected based on their movement at <some date here>', 'H', 22),
		('Iregular Behaviour', 'The user always travels to a location on a Wednesday but they have been on a Tuesday on <some date here>', 'G', 22),
		('Prelonged Inacctivity', 'The user has not moved for over a week', 'A', 22)
;

select * from sent_notifications;



INSERT INTO heart_rate_reading (Reading,UserID,Analysed,Stamp)
VALUES (75, 22, TRUE, date_add(NOW(), INTERVAL 0 MINUTE)),
		(74, 22, TRUE, date_add(NOW(), INTERVAL 1 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL 2 MINUTE)),
        (72, 22, TRUE, date_add(NOW(), INTERVAL 3 MINUTE)),
        (71, 22, TRUE, date_add(NOW(), INTERVAL 4 MINUTE)),
        (72, 22, TRUE, date_add(NOW(), INTERVAL 5 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL 6 MINUTE)),
        (74, 22, TRUE, date_add(NOW(), INTERVAL 7 MINUTE)),
        (75, 22, TRUE, date_add(NOW(), INTERVAL 8 MINUTE)),
        (76, 22, TRUE, date_add(NOW(), INTERVAL 9 MINUTE)),
        (77, 22, TRUE, date_add(NOW(), INTERVAL 10 MINUTE)),
        (78, 22, TRUE, date_add(NOW(), INTERVAL 11 MINUTE)),
        (79, 22, TRUE, date_add(NOW(), INTERVAL 12 MINUTE)),
        (80, 22, TRUE, date_add(NOW(), INTERVAL 13 MINUTE)),
        (81, 22, TRUE, date_add(NOW(), INTERVAL 14 MINUTE)),
        (82, 22, TRUE, date_add(NOW(), INTERVAL 15 MINUTE)),
        (81, 22, TRUE, date_add(NOW(), INTERVAL 16 MINUTE)),
        (80, 22, TRUE, date_add(NOW(), INTERVAL 17 MINUTE)),
        (79, 22, TRUE, date_add(NOW(), INTERVAL 18 MINUTE)),
        (78, 22, TRUE, date_add(NOW(), INTERVAL 19 MINUTE)),
        (79, 22, TRUE, date_add(NOW(), INTERVAL 20 MINUTE))
;

SELECT * FROM heart_rate_reading WHERE UserID = 22 order by Stamp;
DELETE FROM heart_rate_reading WHERE UserID = 22;

INSERT INTO acceleromiter_reading (Reading,UserID,Analysed,Stamp)
VALUES (10, 22, TRUE, date_add(NOW(), INTERVAL 0 MINUTE)),
		(10, 22, TRUE, date_add(NOW(), INTERVAL 1 MINUTE)),
        (10, 22, TRUE, date_add(NOW(), INTERVAL 2 MINUTE)),
        (10, 22, TRUE, date_add(NOW(), INTERVAL 3 MINUTE)),
        (12.5, 22, TRUE, date_add(NOW(), INTERVAL 4 MINUTE)),
        (15.2, 22, TRUE, date_add(NOW(), INTERVAL 5 MINUTE)),
        (2.1, 22, TRUE, date_add(NOW(), INTERVAL 6 MINUTE)),
        (23.4, 22, TRUE, date_add(NOW(), INTERVAL 7 MINUTE)),
        (70.1, 22, TRUE, date_add(NOW(), INTERVAL 8 MINUTE)),
        (13, 22, TRUE, date_add(NOW(), INTERVAL 9 MINUTE)),
        (12.2, 22, TRUE, date_add(NOW(), INTERVAL 10 MINUTE)),
        (11.5, 22, TRUE, date_add(NOW(), INTERVAL 11 MINUTE)),
        (17, 22, TRUE, date_add(NOW(), INTERVAL 12 MINUTE)),
        (22, 22, TRUE, date_add(NOW(), INTERVAL 13 MINUTE)),
        (22.1, 22, TRUE, date_add(NOW(), INTERVAL 14 MINUTE)),
        (21.5, 22, TRUE, date_add(NOW(), INTERVAL 15 MINUTE)),
        (21.9, 22, TRUE, date_add(NOW(), INTERVAL 16 MINUTE)),
        (14, 22, TRUE, date_add(NOW(), INTERVAL 17 MINUTE)),
        (11.3, 22, TRUE, date_add(NOW(), INTERVAL 18 MINUTE)),
        (7.6, 22, TRUE, date_add(NOW(), INTERVAL 19 MINUTE)),
        (0, 22, TRUE, date_add(NOW(), INTERVAL 20 MINUTE))
        ;
SELECT * FROM acceleromiter_reading WHERE UserID = 22;









INSERT INTO heart_rate_reading (Reading,UserID,Analysed,Stamp)
VALUES (75, 22, TRUE, date_add(NOW(), INTERVAL 0 MINUTE)),
		(74, 22, TRUE, date_add(NOW(), INTERVAL -1 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -2 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -3 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -4 MINUTE)),
        (77, 22, TRUE, date_add(NOW(), INTERVAL -5 MINUTE)),
        (80, 22, TRUE, date_add(NOW(), INTERVAL -6 MINUTE)),
        (72, 22, TRUE, date_add(NOW(), INTERVAL -7 MINUTE)),
        (63, 22, TRUE, date_add(NOW(), INTERVAL -8 MINUTE)),
        (85, 22, TRUE, date_add(NOW(), INTERVAL -9 MINUTE)),
        (84, 22, TRUE, date_add(NOW(), INTERVAL -10 MINUTE)),
        (83, 22, TRUE, date_add(NOW(), INTERVAL -11 MINUTE)),
        (81, 22, TRUE, date_add(NOW(), INTERVAL -12 MINUTE)),
        (79, 22, TRUE, date_add(NOW(), INTERVAL -13 MINUTE)),
        (81, 22, TRUE, date_add(NOW(), INTERVAL -14 MINUTE)),
        (79, 22, TRUE, date_add(NOW(), INTERVAL -15 MINUTE)),
        (75, 22, TRUE, date_add(NOW(), INTERVAL -16 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -17 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -18 MINUTE)),
        (73, 22, TRUE, date_add(NOW(), INTERVAL -19 MINUTE)),
        (71, 22, TRUE, date_add(NOW(), INTERVAL -20 MINUTE))
;

INSERT INTO acceleromiter_reading (Reading,UserID,Analysed,Stamp)
VALUES (22.1, 22, TRUE, date_add(NOW(), INTERVAL 0 MINUTE)),
		(18.3, 22, TRUE, date_add(NOW(), INTERVAL -1 MINUTE)),
        (7.4, 22, TRUE, date_add(NOW(), INTERVAL -2 MINUTE)),
        (20.5, 22, TRUE, date_add(NOW(), INTERVAL -3 MINUTE)),
        (25.2, 22, TRUE, date_add(NOW(), INTERVAL -4 MINUTE)),
        (17.3, 22, TRUE, date_add(NOW(), INTERVAL -5 MINUTE)),
        (16.2, 22, TRUE, date_add(NOW(), INTERVAL -6 MINUTE)),
        (17.7, 22, TRUE, date_add(NOW(), INTERVAL -7 MINUTE)),
        (16.6, 22, TRUE, date_add(NOW(), INTERVAL -8 MINUTE)),
        (16.6, 22, TRUE, date_add(NOW(), INTERVAL -9 MINUTE)),
        (16.4, 22, TRUE, date_add(NOW(), INTERVAL -10 MINUTE)),
        (12.8, 22, TRUE, date_add(NOW(), INTERVAL -11 MINUTE)),
        (13, 22, TRUE, date_add(NOW(), INTERVAL -12 MINUTE)),
        (11, 22, TRUE, date_add(NOW(), INTERVAL -13 MINUTE)),
        (10.2, 22, TRUE, date_add(NOW(), INTERVAL -14 MINUTE)),
        (9.3, 22, TRUE, date_add(NOW(), INTERVAL -15 MINUTE)),
        (8.1, 22, TRUE, date_add(NOW(), INTERVAL -16 MINUTE)),
        (7.9, 22, TRUE, date_add(NOW(), INTERVAL -17 MINUTE)),
        (7.6, 22, TRUE, date_add(NOW(), INTERVAL -18 MINUTE)),
        (7.4, 22, TRUE, date_add(NOW(), INTERVAL -19 MINUTE)),
        (7, 22, TRUE, date_add(NOW(), INTERVAL -20 MINUTE))
        ;
        
        
delete from acceleromiter_reading where UserID = 22 and Reading = 70.1;

INSERT INTO authorised_users(Authorised_Time,Expiry_Time,Monitored_UserID,Authorised_UserID, Relation, DataTable)
VALUES (now(), date_add(now(), INTERVAL 1 MONTH),24,25, 'M', 'H'),
		(now(), date_add(now(), INTERVAL 1 MONTH),24,25 , 'M', 'A'),
        (now(), date_add(now(), INTERVAL 1 MONTH),24,25 , 'M', 'G'),
        (now(), date_add(now(), INTERVAL 1 MONTH),24,25 , 'M', 'R')
	;
    






SELECT RuleID, Rule_Name, Action_Name, Action_On, Created_By, Authorised, DataTable
FROM rule r INNER JOIN rule_action ra ON r.Rule_ActionID = ra.Rule_ActionID
WHERE Created_By = 24 AND (Expire_Stamp IS NULL OR Expire_Stamp < '2018-02-15 11:53:19.401');




SELECT RuleID, Rule_Name, Action_Name, Action_On, Created_By, Authorised, DataTable
FROM rule r INNER JOIN rule_action ra ON r.Rule_ActionID = ra.Rule_ActionID\n
WHERE (Created_By = 24  OR Action_On = 24)
	AND (Expire_Stamp IS NULL 
	OR Expire_Stamp < '2018-02-15 11:53:19.401');
    


SELECT * 
FROM authorised_users
WHERE (Authorised_UserID = 24 OR Monitored_UserID = 24)
		AND (Expiry_Time IS NULL )
		-- OR Expiry_Time < '2018-02-15 11:53:19.401')
        
        ;
        
SELECT * FROM authorised_users WHERE( Monitored_UserID = 24 OR Authorised_UserID = 24 ) and Expiry_Time is null;
update authorised_users set Relation = 'D', DataTable = 'H' where Authorised_UsersID = 6;
update authorised_users set Relation = 'D', DataTable = 'A' where Authorised_UsersID = 7;
update authorised_users set Relation = 'D', DataTable = 'G' where Authorised_UsersID = 8;   



select * from rule_action;

select * from users;

update users set User_Type = 'D' where User_Type = 'Monitored'; -- set to default
update users set User_Type = 'M' where User_Type = 'Doctor'; -- set to Medical professional
update users set User_Type = 'F' where User_Type = 'Relative'; -- set to Family member

select * from rule;

update rule set DataTable = 'H' where RuleID = 2;